from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from src.database import session, Booking
from os import access
from flask import Blueprint, app, request, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
import validators
from flask_jwt_extended import current_user, jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from flasgger import swag_from
from varname import nameof

bookings = Blueprint("bookmarks", __name__, url_prefix="/api/v1/bookings")


@bookings.post('/')
@jwt_required()
@swag_from('./docs/booking/addbooking.yaml')
def handle_booking():
    user_id = get_jwt_identity()
    check = request.json
    req_var = ['uuid', 'total', 'type', 'total_with_discount',
               'payment_status', 'service_status']
    for req in req_var:
        if req not in check:
            return jsonify({
                "error": "Wrong Format... '{}' is missing".format(req)
            })

    uuid = request.json['uuid']
    total = request.json['total']
    type = request.json['type']
    total_with_discount = request.json['total_with_discount']
    payment_status = request.json['payment_status']
    service_status = request.json['service_status']

    # Non Required Fields

    NRF = ["payment_method_id", "training_id", "batch_id", "batch_date", "service_required_time", "service_required_date", "payment_response",
           "payment_request", "training_id", "address_id", "transaction_id", "message_for_provider", "service_engineer_id", "created_at",
           "updated_at", "otp", "method_charges", "commission", "tax", "tax_value", "commission_value", "total_after_discount", "discount",
           "discount_value", "coupon_type", "coupon_amount", "calculated_coupon_amount"]

    globals()['payment_method_id'] = None
    globals()['training_id'] = None
    globals()['batch_id'] = None
    globals()['batch_date'] = None
    globals()['service_required_time'] = None
    globals()['service_required_date'] = None
    globals()['payment_response'] = None
    globals()['payment_request'] = None
    globals()['transaction_id'] = None
    globals()['address_id'] = None
    globals()['message_for_provider'] = None
    globals()['service_engineer_id'] = None
    globals()['created_at'] = None
    globals()['updated_at'] = None
    globals()['otp'] = None
    globals()['method_charges'] = None
    globals()['commission'] = None
    globals()['tax'] = None
    globals()['tax_value'] = None
    globals()['commission_value'] = None
    globals()['total_after_discount'] = None
    globals()['discount'] = None
    globals()['discount_value'] = None
    globals()['coupon_type'] = None
    globals()['coupon_amount'] = None
    globals()['calculated_coupon_amount'] = None

    for non in NRF:
        if non in check:
            tmp = non
            globals()[tmp] = request.json[tmp]

    session.add(
        Booking(uuid=uuid, total=total, type=type, total_with_discount=total_with_discount, payment_status=payment_status,
                service_status=service_status, payment_method_id=payment_method_id, user_id=user_id, training_id=training_id,
                batch_id=batch_id, batch_date=batch_date, service_required_time=service_required_time,
                service_required_date=service_required_date, payment_response=payment_response, payment_request=payment_request,
                transaction_id=transaction_id, address_id=address_id, message_for_provider=message_for_provider,
                service_engineer_id=service_engineer_id, created_at=created_at, updated_at=updated_at, otp=otp, method_charges=method_charges,
                commission=commission, tax=tax, tax_value=tax_value, commission_value=commission_value,
                total_after_discount=total_after_discount, discount=discount, discount_value=discount_value, coupon_type=coupon_type,
                coupon_amount=coupon_amount, calculated_coupon_amount=calculated_coupon_amount
                ))
    session.commit()

    return jsonify({
        "Success": "Booking Added"
    }), HTTP_200_OK


@bookings.get('/')
@jwt_required()
@swag_from('./docs/booking/getbooking.yaml')
def handle_bookings():
    user_id = get_jwt_identity()

    if request.method == 'GET':
        # Get Booking Information
        user_id = get_jwt_identity()
        book = session.query(Booking).filter(
            Booking.user_id == user_id)

        booking_list = []

        for booking in book:
            booking_list.append({
                "id": booking.id,
                "uuid": booking.uuid,
                "total": booking.total,
                "type": booking.type,
                "total_with_discount": booking.total_with_discount,
                "payment_status": booking.payment_status,
                "service_status": booking.service_status,
                'payment_method_id': booking.payment_method_id,
                'batch_id': booking.batch_id,
                'training_id': booking.training_id,
                'batch_date': booking.batch_date,
                'service_required_time': booking.service_required_time,
                'service_required_date': booking.service_required_date,
                'payment_response': booking.payment_response,
                'payment_request': booking.payment_request,
                'transaction_id': booking.transaction_id,
                'address_id': booking.address_id,
                'message_for_provider': booking.message_for_provider,
                'service_engineer_id': booking.service_engineer_id,
                'created_at': booking.created_at,
                'updated_at': booking.updated_at,
                'otp': booking.otp,
                'method_charges': booking.method_charges,
                'commission': booking.commission,
                'tax': booking.tax,
                'tax_value': booking.tax_value,
                'commission_value': booking.commission_value,
                'total_after_discount': booking.total_after_discount,
                'discount': booking.discount,
                'discount_value': booking.discount_value,
                'coupon_type': booking.coupon_type,
                'coupon_amount': booking.coupon_amount,
                'calculated_coupon_amount': booking.calculated_coupon_amount

            })
        return jsonify({
            "Bookings": booking_list
        }), HTTP_200_OK


@bookings.delete('/<int:id>')
@jwt_required()
@swag_from('./docs/booking/delbooking.yaml')
def delete_bookings(id):
    currunt_user = get_jwt_identity()

    book = session.query(Booking).filter_by(user_id=current_user, id=id)

    if not book:
        return jsonify({
            "error": "Item Not Found"
        })

    book.service_status = 4
    # session.delete(book)
    session.commit()

    return jsonify({
        "Sucess": "Successfully Deleted"
    }), HTTP_200_OK


@bookings.put('/change_date_time/<int:id>')
@jwt_required()
@swag_from('./docs/booking/change_date.yaml')
def change_date_time(id):
    current_user = get_jwt_identity()
    book = session.query(Booking).filter_by(user_id=current_user, id=id)

    if not book:
        return jsonify({
            "error": "Item Not Found"
        })

    service_required_time = request.json.get('service_required_time', '')
    service_required_date = request.json.get('service_required_date', '')

    book.service_required_time = service_required_time
    book.service_required_date = service_required_date

    session.commit

    return jsonify({"Updated": "Subscription Updated"})
