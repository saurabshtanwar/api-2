from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from src.database import session, User, User_addresses
from os import access
from flask import Blueprint, app, request, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
import validators
from flask_jwt_extended import current_user, jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from flasgger import swag_from


auth = Blueprint("auth", __name__, url_prefix="/api/v1/auth")


@auth.post('/register')
@swag_from('./docs/auth/register.yaml')
def register():
    name = request.json['name']
    email = request.json['email']
    password = request.json['password']
    mobile = request.json['mobile']

    if len(password) < 6:
        return jsonify({"error": "Password is too short"}), HTTP_400_BAD_REQUEST

    if len(name) < 3:
        return jsonify({"error": "Name is too short"}), HTTP_400_BAD_REQUEST

    if not name.isalnum():
        return jsonify({"error": "Name Needs To be alphanumeric"}), HTTP_400_BAD_REQUEST

    if len(mobile) != 10:
        return jsonify({"error": "Enter a Valid Mobile Number"}), HTTP_400_BAD_REQUEST

    if not validators.email(email):
        return jsonify({"error": "Enter a Valid Email"}), HTTP_400_BAD_REQUEST

    email_check = session.query(User).filter(User.email == email)
    email_length = email_check.all()

    if not len(email_length) == 0:
        return jsonify({"error": "Email already exixts"}), HTTP_409_CONFLICT

    mobile_check = session.query(User).filter(User.mobile == mobile)
    mobile_length = mobile_check.all()

    if not len(mobile_length) == 0:
        return jsonify({"error": "mobile no. already exixts"}), HTTP_409_CONFLICT

    pwd_hash = generate_password_hash(password)

    session.add(User(name=name, email=email, password=pwd_hash, mobile=mobile))
    session.commit()

    user = session.query(User).filter(User.mobile == mobile).first()

    user_id = user.id
    return jsonify({"messge": "User created",
                    "user": {
                        "name": name,
                        "email": email,
                        "user_id": user_id
                    }}), HTTP_201_CREATED


@auth.post('/login')
@swag_from('./docs/auth/login.yaml')
def login():
    email = request.json.get('email', '')
    password = request.json.get('password', '')

    user = session.query(User).filter(User.email == email).first()

    if user:
        is_pass_correct = check_password_hash(user.password, password)
        if is_pass_correct:
            refresh = create_refresh_token(identity=user.id)
            access = create_access_token(identity=user.id)
            return jsonify({
                'user': {
                    'refresh': refresh,
                    'access': access,
                    'name': user.name,
                    'email': user.email
                }
            }), HTTP_200_OK

    return jsonify({"error": "Wrong Credentials"}), HTTP_401_UNAUTHORIZED


@auth.get('/me')
@jwt_required()
@swag_from('./docs/auth/me.yaml')
def me():
    user_id = get_jwt_identity()
    user = session.query(User).filter(User.id == user_id).first()

    return jsonify({
        "name": user.name,
        "email": user.email
    }), HTTP_200_OK


@auth.get('/token/refresh')
@jwt_required(refresh=True)
def refresh_user_token():
    identity = get_jwt_identity()
    access = create_access_token(identity=identity)

    return jsonify({
        'access': access
    }), HTTP_200_OK


@auth.put('/change_password')
@jwt_required()
@swag_from('./docs/auth/changepassword.yaml')
def edituser():
    current_user = get_jwt_identity()
    pre_password = request.json.get('old_password', '')
    new_password = request.json.get('new_password', '')
    confirm_password = request.json.get('confirm_password', '')
    user = session.query(User).filter(User.id == current_user).first()

    is_pass_correct = check_password_hash(user.password, pre_password)

    if len(new_password) < 6:
        return jsonify({"error": "Password is too short"}), HTTP_400_BAD_REQUEST

    if is_pass_correct:
        if new_password == confirm_password:
            pwd_hash = generate_password_hash(new_password)
            user.password = pwd_hash
            session.commit()
            return jsonify({
                "Success": "Password changed Successfully"
            })
        else:
            return jsonify({
                "error": "Password Does Not Match"
            })
    else:
        return jsonify({
            "error": "Wrong Password"
        })


@auth.put('/is_online')
@jwt_required()
@swag_from('./docs/auth/is_online.yaml')
def is_online():
    current_user = get_jwt_identity()
    user = session.query(User).filter(User.id == current_user).first()

    user.is_online = 1
    session.commit

    return jsonify({"Online": "1"})


@auth.put('/is_offline')
@jwt_required()
@swag_from('./docs/auth/is_online.yaml')
def is_offline():
    current_user = get_jwt_identity()
    user = session.query(User).filter(User.id == current_user).first()

    user.is_online = 0
    session.commit

    return jsonify({"Online": "1"})


@auth.put('/subscription/<int:id>')
@swag_from('./docs/auth/subscription.yaml')
def subs(id):
    current_user = id
    user = session.query(User).filter(User.id == current_user).first()

    subscription_id = request.json.get('subscription_id ', '')
    subscription_price = request.json.get('subscription_price', '')

    user.subscription_id = subscription_id
    user.subscription_price = subscription_price

    session.commit

    return jsonify({"Updated": "Subscription Updated"})


@auth.get('/subscription')
@jwt_required()
@swag_from('./docs/auth/get_subs.yaml')
def get_subscriptions():
    current_user = get_jwt_identity()
    user = session.query(User).filter(User.id == current_user).first()

    subscription_id = user.subscription_id
    subscription_price = user.subscription_price
    session.commit

    return jsonify({
        "Subscription_ID": subscription_id,
        "subscription_price": subscription_price
    })
