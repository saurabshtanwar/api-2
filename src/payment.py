from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from src.database import session, Booking, Booking_Services, User
from os import access
from flask import Blueprint, app, request, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
import validators
from flask_jwt_extended import current_user, jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from flasgger import swag_from
from varname import nameof

payments = Blueprint("payments", __name__, url_prefix="/api/v1/payments")


@payments.post('/?Booking_id=<int:booking_id>')
@jwt_required()
@swag_from('./docs/payment/postpayment.yaml')
def post_payment(booking_id):
    user_id = get_jwt_identity()
    price = request.json['price']
    cart_id = request.json['cart_id']
    service_id = request.json['service_id']
    quantity = request.json['qantity']
    payment_status = request.json['payment_status']
    discount = request.json['discount']
    payment_method_id = request.json['payment_method_id']
    total_with_discount = request.json['total_with_discount']
    transaction_id = request.json['transaction_id']

    book = session.query(Booking).filter_by(
        user_id=user_id, id=booking_id)

    if not book:
        return jsonify({
            "error": "Item Not Found"
        }), HTTP_400_BAD_REQUEST
    type = ""
    for booking in book:
        type = booking.type
    session.add(Booking_Services(booking_id=booking_id,
                service_id=service_id, type=type, price=price, quantity=quantity))

    for booking in book:
        booking.payment_method_id = payment_method_id
        booking.total_with_discount = total_with_discount
        booking.discount = discount
        booking.payment_status = payment_status
        booking.transaction_id = transaction_id
        booking.total = price

    session.commit()

    return jsonify({
        "Success": "Payment Updated"
    }), HTTP_200_OK


@payments.get('/')
@jwt_required()
@swag_from('./docs/payment/getpayments.yaml')
def get_payment():
    user_id = get_jwt_identity()
    book = session.query(Booking).filter_by(
        user_id=user_id)
    user = session.query(User).filter_by(id=user_id).first()

    bookings = []
    for booking in book:
        bookings.append({
            "username": user.name,
            "email": user.email,
            "User_id": user.id,
            "service_id": booking.service_id,
            "discount": booking.discount,
            "type": booking.type,
            "total": booking.total,
            "discount": booking.discount,
            "total_with_discount": booking.total_with_discount
        })

    return jsonify({
        "payment history": bookings
    })
