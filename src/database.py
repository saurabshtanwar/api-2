from sqlalchemy import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
import os

engine = create_engine(os.environ.get("SQLALCHEMY_DB_URI"))
Base = automap_base()
Base.prepare(engine, reflect=True)


User = Base.classes.users
User_addresses = Base.classes.user_addresses
Booking = Base.classes.bookings
Booking_Services = Base.classes.booking_services
Booking_Requests = Base.classes.booking_requests


# Home
Services = Base.classes.services
Classes = Base.classes.classes
Basis_website_details = Base.classes.basic_website_details

# Wallet
wallet_transactions = Base.classes.wallet_transactions
wallets = Base.classes.wallets

# Blogs
Blogs = Base.classes.blogs

session = Session(engine)
