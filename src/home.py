from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from src.database import session, Services, Classes, User, Basis_website_details
from os import access
from flask import Blueprint, app, request, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
import validators
from flask_jwt_extended import jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from flasgger import swag_from


home = Blueprint("home", __name__, url_prefix="/api/v1/home")


@home.get('/')
@swag_from('./docs/home/home.yaml')
def home_page():

    basic_details_list = []
    basic_w_d = session.query(Basis_website_details)

    for details in basic_w_d:
        basic_details_list.append({
            "name": details.name,
            "Contact": details.address,
            "Social": details.social_links
        })

    facilities_list = []
    facility = session.query(Services).filter(Services.type == 0)

    for fac in facility:
        facilities_list.append({
            "service_name": fac.service_name,
            "description": fac.description,
            "price": fac.price,
            "short_description": fac.short_description,
            "slug": fac.slug
        })

    training_list = []
    training = session.query(Classes)

    for train in training:
        training_list.append({
            "training_name": train.name,
            "description": train.description,
            "price": train.price,
            "slug": train.slug
        })

    shop_list = []
    shop_products = session.query(Services).filter(Services.type == 1)
    for product in shop_products:
        shop_list.append({
            "service_name": product.service_name,
            "description": product.description,
            "price": product.price,
            "short_description": product.short_description,
            "slug": product.slug
        })

    return jsonify({
        "facilities": facilities_list,
        "Training": training_list,
        "Basic Website Details": basic_details_list,
        "Shop": shop_list
    }), HTTP_200_OK
