from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from src.database import Booking, session, User, User_addresses, Blogs
from os import access
from flask import Blueprint, app, request, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
import validators
from flask_jwt_extended import current_user, jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from flasgger import swag_from

blog_ = Blueprint("blogs", __name__, url_prefix="/api/v1/blogs")


@blog_.get('/')
@swag_from('./docs/blogs/blogs.yaml')
def bloge():

    blog = session.query(Blogs)

    blog_list = []
    for bin in blog:
        blog_list.append({
            "id": bin.id,
            "slug": bin.slug,
            "service_name": bin.service_name,
            "description": bin.description,
            "visit": bin.visit,
            "keywords": bin.keywords,
            "created_at": bin.created_at,
            "updated_at ": bin.updated_at,
            "short_description": bin.short_description
        })

    return jsonify({
        "blogs": blog_list
    })
