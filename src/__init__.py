# Imports
from flask.json import jsonify
from src.constants.http_status_codes import HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR
from flask import Flask, config, redirect
import os
from src.auth import auth, register
from src.booking import bookings
from src.database import session
from src.blogs import blog_
from src.home import home
from src.payment import payments
from flask_jwt_extended import JWTManager
from flasgger import Swagger, swag_from
from src.config.swagger import template, swagger_config


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        app.config.from_mapping(
            SECRET_KEY=os.environ.get("SECRET_KEY"),
            SQLALCHEMY_DATABASE_URI=os.environ.get("SQLALCHEMY_DB_URI"),
            SQLALCHEMY_TRACK_MODIFICATIONS=False,
            JWT_SECRET_KEY=os.environ.get('JWT_SECRET_KEY'),


            SWAGGER={
                'title': "Facility API",
                'uiversion': 3
            },

        )
    else:
        app.config.from_mapping(test_config)

    JWTManager(app)
    app.register_blueprint(auth)
    app.register_blueprint(bookings)
    app.register_blueprint(home)
    app.register_blueprint(blog_)
    app.register_blueprint(payments)

    Swagger(app, config=swagger_config, template=template)

    @app.get("/")
    def index():
        return("Hello World")

    @app.get("/hello")
    def hello():
        return jsonify({"message": "Hello World"})

    return app
